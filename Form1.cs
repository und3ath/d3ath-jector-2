﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using Mono.Cecil;
using ICSharpCode.Decompiler.Ast;
using ICSharpCode.Decompiler;
using System.IO;
using ColorCode;

namespace d3ath_jector2
{
    public partial class Form1 : Form
    {

        public string InputPEPath = string.Empty;
        public MonoWrapper MonoCecil = null;
        static ImageList _imageList;
        public bool CSharpOutPut { get; set; }

        public Form1()
        {
            InitializeComponent();
            MonoCecil = new MonoWrapper();
            CSharpOutPut = true;
            treeView_peTree.ImageList = Form1.ImageList;
            
        }

       
        public static ImageList ImageList
        {
            get
            {
                if (_imageList == null)
                {
                    _imageList = new ImageList();
                    _imageList.Images.Add("Class", Properties.Resources.Class);
                    _imageList.Images.Add("Methods", Properties.Resources.Method);
                }
                return _imageList;
            }
        }


        private void button_openPE_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDlg = new OpenFileDialog();
            fileDlg.Filter = "Executables | *.exe";
            if(fileDlg.ShowDialog() == DialogResult.OK)
            {
                InputPEPath = fileDlg.FileName;
                textBox_inputPath.Text = InputPEPath;
                if (MonoCecil.LoadPEFromPath(InputPEPath))
                {
                    ParsePeInfos();
                    ParsePeTree();
                }
            }
            
        }

        private void ParsePeInfos()
        {
            listBox_PeInfos.Items.Add("Name: " + MonoCecil.LoadedAssemblyDef.MainModule.Name);
            listBox_PeInfos.Items.Add("Runtime: " + MonoCecil.LoadedAssemblyDef.MainModule.Runtime.ToString());
            listBox_PeInfos.Items.Add("Metadata Token: " + MonoCecil.LoadedAssemblyDef.MetadataToken.ToString());
            listBox_PeInfos.Items.Add("Architecture: " + MonoCecil.LoadedAssemblyDef.MainModule.Architecture.ToString());
            listBox_PeInfos.Items.Add("EntryPoint: " + MonoCecil.LoadedAssemblyDef.MainModule.EntryPoint.ToString());
        }


        private void ParsePeTree()
        {
            TreeNode rootNode;
            IEnumerator typesEnumerator = MonoCecil.LoadedAssemblyDef.MainModule.Types.GetEnumerator();
            while(typesEnumerator.MoveNext())
            {
                TypeDefinition typeDef = (TypeDefinition)typesEnumerator.Current;
                rootNode = treeView_peTree.Nodes.Add(typeDef.Name);
                rootNode.ImageKey = "Class";
                rootNode.SelectedImageKey = "Class";

                IEnumerator methodsEnumerator = typeDef.Methods.GetEnumerator();
                while(methodsEnumerator.MoveNext())
                {
                    MethodDefinition methodDef = (MethodDefinition)methodsEnumerator.Current;
                    TreeNode n1 = new TreeNode(methodDef.Name);
                    n1.ImageKey = "Methods";
                    n1.SelectedImageKey = "Methods";
                    rootNode.Nodes.Add(n1);
                                    
                }
            }
        }


        private void ParseCode()
        {
            IEnumerator typesEnumerator = MonoCecil.LoadedAssemblyDef.MainModule.Types.GetEnumerator();
            while(typesEnumerator.MoveNext())
            {
                TypeDefinition typeDef = (TypeDefinition)typesEnumerator.Current;
                if (treeView_peTree.SelectedNode.Parent.Text == typeDef.Name)
                {
                    IEnumerator methodsEnumerator = typeDef.Methods.GetEnumerator();
                    while (methodsEnumerator.MoveNext())
                    {
                        MethodDefinition methodDef = (MethodDefinition)methodsEnumerator.Current;
                        if(treeView_peTree.SelectedNode.Text == methodDef.Name)
                        {

                            //richTextBox_peDisass.Clear();
                            webBrowser1.DocumentText = "";

                            string result = string.Empty;
                            if (CSharpOutPut)
                            {
                                result = MonoCecil.DecompilMethodToCSharpPlainText(MonoCecil.LoadedAssemblyDef.MainModule, typeDef, methodDef);
                                result = new CodeColorizer().Colorize(result, Languages.CSharp);
                            }
                            else
                            {
                                result = MonoCecil.DecompilMethodToILPlainText(methodDef);
                                result = result.Replace("\n", "<br>");
                            }

                            webBrowser1.DocumentText = "<html>" + result + "</html>";

                           // richTextBox_peDisass.Text = result;

                        }
                    }
                }
            }
        }

   



        private void treeView_peTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            ParseCode();
        }

        private void iLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CSharpOutPut = false;
        }

        private void cToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CSharpOutPut = true;
        }

        private void button_validateMsil_Click(object sender, EventArgs e)
        {
            List<string> errors;
            if(!MonoCecil.ValidateInputCSharpToMsil(richTextBox_msilInput.Text, out errors, textBox_referencedAssembly.Lines))
            {
                label_msilValidateStatus.Text = string.Join(",", errors.ToArray());
                label_msilValidateStatus.ForeColor = Color.Red;
            }
            else
            {
                label_msilValidateStatus.Text = "OK.";
                label_msilValidateStatus.ForeColor = Color.Green;
            }
        }

    }
}
