﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Mono.Cecil;
using Mono.Cecil.Cil;
using System.Collections;
using ICSharpCode.Decompiler;
using ICSharpCode.Decompiler.Ast;
using Microsoft.CSharp;
using System.CodeDom;
using System.CodeDom.Compiler;

namespace d3ath_jector2
{

  

    public class MonoWrapper
    {
        public AssemblyDefinition LoadedAssemblyDef { get; set; }
        private AstBuilder astBld { get; set; }

        public MonoWrapper()
        {

        }

        public bool LoadPEFromPath(string filePath)
        {
            try
            {
                LoadedAssemblyDef = AssemblyDefinition.ReadAssembly(filePath);
            }
            catch(Exception e)
            {
                MessageBox.Show("Error loading assembly from path, probably encrypted or non .net assembly." +
                    Environment.NewLine +
                    "Error: " +
                    Environment.NewLine +
                    e.Message);
                LoadedAssemblyDef = null;
                return false;
            }
            return true;
        }

        public string DecompilMethodToCSharpPlainText(ModuleDefinition moduleDef, TypeDefinition typeDef, MethodDefinition methodDef)
        {
            DecompilerContext decompilerContext = new DecompilerContext(moduleDef);
            decompilerContext.CurrentType = typeDef;
            astBld = new AstBuilder(decompilerContext);
            astBld.AddMethod(methodDef);


            StringWriter stringWritter = new StringWriter();
            astBld.GenerateCode(new PlainTextOutput(stringWritter));
            string result = stringWritter.ToString();
            stringWritter.Dispose();
            return result;
        }

        public string DecompilMethodToILPlainText(MethodDefinition methodDef)
        {
            string result = "";
            ILProcessor cilProcessor = methodDef.Body.GetILProcessor();           
            foreach(Instruction inst in cilProcessor.Body.Instructions)
            {
                result += inst;
                result += Environment.NewLine;
            }
            return result;
        }


        public bool ValidateInputCSharpToMsil(string csharp, out List<string> compilEx, string[] referencedAssembly)
        {
            compilEx = new List<string>();
            CompilerResults compRes = CompileFromSource(csharp, referencedAssembly);    
            if(compRes.Errors.Count == 0)
            {
                try
                {
                    AssemblyDefinition def = AssemblyDefinition.ReadAssembly(compRes.PathToAssembly);                   
                }
                catch(Exception e)
                {
                    compilEx.Add(e.Message);
                    return false;
                }
            }
            else
            {
                foreach(CompilerError err in compRes.Errors)
                {                    
                    compilEx.Add(err.ErrorText);
                    return false;
                }
                
            }
            return true;
        }


        private List<Instruction> DumpMethodIL(MethodDefinition methodDef)
        {
            if (!methodDef.HasBody)
                throw new Exception();
            ILProcessor cilProc = methodDef.Body.GetILProcessor();
            List<Instruction> methodBody = new List<Instruction>();
            methodBody.AddRange(cilProc.Body.Instructions);
            return methodBody;
        }

        private CompilerResults CompileFromSource(string csharp, string[] referencedAssembly)
        {
            CSharpCodeProvider provider = new CSharpCodeProvider();
            CompilerParameters Compilparam = new CompilerParameters {
                GenerateExecutable = false,
                GenerateInMemory = false,
                IncludeDebugInformation = false
            };
            Compilparam.ReferencedAssemblies.AddRange(referencedAssembly.ToArray());
            return provider.CompileAssemblyFromSource(Compilparam, csharp);
        }
        

    }
}
